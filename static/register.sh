#!/bin/bash
gitlab-runner register \
                --non-interactive \
                --executor "docker" \
                --docker-image alpine:latest \
                --url "${GITLAB_URL}" \
                --registration-token "${REGISTRATION_TOKEN}" \
                --description "docker-runner, ID: ${RUNNER_ID}" \
                --tag-list "docker" \
                --run-untagged="true" \
                --locked="true" \
                --access-level="not_protected"
