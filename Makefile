.DEFAULT_GOAL := help
.PHONY: help test attach register restart stop start
MAKEFILE_DIR := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
help: ## Displays this help message
	@grep -E '^[a-zA-Z0-9_-]+?:[ a-zA-Z0-9_-]*?##.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## *"}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

# ------- Docker commands ------- #

start: ## bring docker-compose up
	@cd $(MAKEFILE_DIR) && docker-compose up -d

stop: ## bring all docker-compose services down
	@cd $(MAKEFILE_DIR) && docker-compose down

restart: ## Restart all services in docker-compose.yml
	@cd $(MAKEFILE_DIR) && docker-compose restart

attach: ## Attach to gitlab runner to run commands
	@cd $(MAKEFILE_DIR) && docker-compose exec gitlab-runner /bin/bash

# ------- Initial Setup ------- #

register: ## Registers the gitlab runner. Run this once you have filled out the .env from the env command!
	@cd $(MAKEFILE_DIR) && docker-compose run --rm --entrypoint /bin/bash gitlab-runner /register.sh

env: ## Run this first, and fill out the .env file that it creates!! (WARNING: .env must not already exist)  
	@cd $(MAKEFILE_DIR) && \
		[ -f .env ] \
		&& echo ".env already exist. Please remove it and try again." \
		|| { cp static/env_template .env && echo "Created .env"; }
